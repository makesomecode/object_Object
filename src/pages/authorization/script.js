import scrollBar from 'vue-perfect-scrollbar'

export default {
    name: 'authorization',

  data: function () {
    return {
        wHeight: 0,
        allMessages: [
          {messages: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage:true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author",  newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author",  newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author",  newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author",  newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},
          {message: "test test test test test test test test ", date: "12.06.18, 14:36", author:"Some author", newMessage: true},

        ]
    }},

  created(){
     this.getWindowHeight()
   },

    components: {
      scrollBar
    },

    methods: {

      getWindowHeight(){
       this.wHeight =  window.innerHeight;

      }

    }
};
