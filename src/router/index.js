import Vue from 'vue'
import Router from 'vue-router'
import authorization from "../pages/authorization/authorization"
import orderlist from "../pages/orderlist/orderlist"

//Чтобы подключить страницу (не забудь прописать страницу в routes)
//@import pageName from  'path to pageName'

//чтобы подключить компонент на какую-либо страницу, необходимо его создать или загрузить.
//После этого на нужной странице подключить через @import и после поля data написать - components: { componentName1, [componentName2, ...] }

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'authorization',
      component: authorization,
    },
    {
      path: '/orderlist',
      name: 'orderlist',
      component: orderlist,
    }
  ],
   mode: 'history'
})
